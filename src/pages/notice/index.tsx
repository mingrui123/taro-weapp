import { View, Text, ScrollView  } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import { useState } from 'react'
import MIcon from '../../components/Micon'
import NullComponent from '../../components/NullComponent'
import { noticeSvg, teahouseSvg, memberSvg } from '../../assets/icons'

const noticeList = [
  { label: '系统通知', key: 'notice', svg:  noticeSvg, width: '2em'},
  { label: '茶室', key: 'teahouse', svg: teahouseSvg },
  { label: '会员', key: 'member', svg: memberSvg,  width: '2em' }
];


const NoticeItem = () => {

  return (
    <View className='noticeItem'>
      <View className='noticeItem-img'>
        <MIcon svg={noticeSvg} width='2em' height='2em'></MIcon>
        <View className='noticeItem-label'>官方</View>
      </View>
      <View className='noticeItem-body'>
        <View className='noticeItem-body-title'>
          <View className='fontSize32'>五一限时活动</View>
          <View className='app-gary'>一周前</View>
        </View>
        <View>
          <Text className='app-gary2'>【未读】</Text>
          39.6元享4个小时，折后每小时9.9元
        </View>
      </View>
    </View>
  );
}


/**
 * 消息
 * @returns 
 */
export default function Index() {
  // 优惠列表
  const [list, updateList] = useState([1]);

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='notice'>
     
     <View className='notice-header'>
      {/* <View className='notice-header-tab'> */}
        {
          noticeList.map(r => (
            <View key={r.key} className='notice-header-tab'>
              <View>
                <MIcon svg={r.svg} width={r.width || '2.3em'} height={r.width || '2.3em'}></MIcon>
              </View>
              <Text>{r.label}</Text>
              <Text className='notice-header-tab__num app-notice-num'>12</Text>
            </View>
          ))
        }
      {/* </View> */}
     </View>

      <View className='notice-main'>
        <ScrollView scrollY>
          {
            list.length? (
              Array(5).fill(1).map(() => <NoticeItem></NoticeItem>)
            ): <NullComponent text="没有更多消息"></NullComponent>
          }
        </ScrollView>
      </View>
    </View>
  )
}
