export default defineAppConfig({
  pages: [
    'pages/authorisation/index',
    'pages/index/index',
    'pages/recharge/index',
    
    'pages/notice/index',
    'pages/preferential/index',
    'pages/orderSettlement/index',
    'pages/storeDetails/index',
    'pages/intro/index',
    'pages/my/index',
    'pages/orderDetails/index',
    'pages/orderForm/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    color: '#999999',
    selectedColor: '#B73730',
    backgroundColor: 'white',
    list: [
      {
        pagePath: 'pages/index/index',
        text: '首页'
      },
      {
        pagePath: 'pages/preferential/index',
        text: '优惠'
      },
      {
        pagePath: 'pages/notice/index',
        text: '消息'
      },
      {
        pagePath: 'pages/my/index',
        text: '我的'
      },
    ]
  }
})
