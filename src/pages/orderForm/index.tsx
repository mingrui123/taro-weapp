import { View, Text } from '@tarojs/components'
import Taro, { useLoad, getCurrentInstance } from '@tarojs/taro'
import './index.less'
import { useState } from 'react'

const tabList = [
  { label: '全部', key: 'all' },
  { label: '待付款', key: 'obligation' },
  { label: '待使用', key: 'toBeUse' },
  { label: '使用中', key: 'inUse' },
  { label: '已完成', key: 'done' },
  { label: '已取消', key: 'cancel' },
  { label: '已退款', key: 'refunded' },
];

const OrderItem = () => {

  return (
    <View className='order-item' onClick={handleNavigateOrderDetails}>
      <View className='order-item-header'>
        <View className='order-header-title'>
          <View>V01（棋牌室 茶室）</View>
          <View>【demo】惠州博物啊实打实</View>
        </View>
        <View className='order-header-state'>已取消</View>
      </View>

      <View className='order-item-main'>
        <View className='order-main-adress'>
          地址：广东省惠州市博罗县罗阳街道赛头创享大厦A栋502室
        </View>
        <View className='order-main-usagetime'>
          使用时间：2023-05-05&nbsp;&nbsp;&nbsp;14:30 ~ 05-05 16:30
        </View>
        <View className='order-main-time'>
          使用时间：2小时0分钟
        </View>
        <View className='order-main-money'>
          实付金额：￥63.20
        </View>
      </View>
    </View>  
  );

  function handleNavigateOrderDetails () {
    Taro.navigateTo({
      url: "/pages/orderDetails/index"
    })
  }
}

export default function Index() {
  const params = getCurrentInstance()?.router?.params || {};

  const [tabIndex, updateTab] = useState<string>(params.tab || 'all');

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='order'>
      <View className='order-header'>
        <View className='order-header-tab'>
          {
            tabList.map(res => (
              <View key={res.key} 
                className={tabIndex === res.key? 'tab-check': ''}
                onClick={handleCheckTab.bind(null, res.key)}
              >
                <Text>{ res.label }</Text>
                <View></View>
              </View>
            ))
          }
        </View>
      </View>

      <View className='order-main'>
        <OrderItem></OrderItem>
      </View>

      <View className='order-footer'></View>
    </View>
  )

  function handleCheckTab (tab) {
    updateTab(tab)
  }
}
