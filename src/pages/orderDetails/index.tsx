import { View, Text, Image } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import MIcon from '../../components/Micon'
import { customerServiceSvg, phoneSvg } from "../../assets/icons"

const imgUrl = 'https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f';

export default function Index() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='order-details'>
      <View className='order-details-header' onClick={handleNavigateStoreDetails}>
        <View className='order-details-header-state'>订单状态: 已取消</View>
        <View className='order-details-header-mess'>
          <Image src={imgUrl}></Image>
          <View>
            <View className='order-details__title'>VO1 (棋牌室 茶室)</View>
            <View className='order-details__body'>
              <View>广东省惠州市博罗县罗阳啊实打实的</View>
              <View>自营</View>
            </View>
            <View className='order-details__time'>2023-05-05  14:30~05-05  16:30</View>
          </View>
        </View>
      </View>

      <View className='order-details-main'>
        <View className='order-details-main-title'>订单详情</View>
        <View className='order-details-main-body'>
          <View>
            <Text>订单号</Text>
            <View className='flex'>
              <View className='bold'>H202305051152077003</View>
              <View className='app-but_border margin-left10'>复制</View>
            </View>
          </View>
          <View>
            <Text>实付金额</Text>
            <View className='app-red bold'>￥63.20</View>
          </View>
          <View>
            <Text>支付方式</Text>
            <View>微信支付</View>
          </View>
          <View>
            <Text>服务时长</Text>
            <View>2小时0分钟</View>
          </View>
          <View>
            <Text>下单时间</Text>
            <View>2023-05-05 11:52:07</View>
          </View>
          
        </View>
      </View>

      <View className='order-footer'>
        <View className='app-red app-but margin-bottom20 alignCenter'>
          <MIcon svg={customerServiceSvg} width='20px' height='20px'></MIcon> 在线客服</View>
        <View className='app-red app-but'>
          <MIcon svg={phoneSvg} width='20px' height='20px'></MIcon> 联系商家</View>
      </View>
    </View>
  )

  function handleNavigateStoreDetails () {
    Taro.navigateTo({
      url: "/pages/storeDetails/index"
    })
  }
}
