import { View, Text, Image } from '@tarojs/components'
import Taro, { useLoad, getCurrentInstance } from '@tarojs/taro'
import './index.less'
import { vipSvg, rightGarySvg, paymentSvg, wxPaySvg, checkSvg } from '../../assets/icons'
import MIcon from '../../components/Micon'
import { AtCheckbox } from 'taro-ui'
import {useState} from 'react'

const imgUrl = 'https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f';

export default function Index() {
  const params = getCurrentInstance()?.router?.params || {};

  const [paymentType, updatePaymentType] = useState<'wx' | 'cx'>('wx');

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='orderSettlement'>
      <View className='orderSettlement-header'>
        <View className='orderSettlement-header-imgMess'>
          <Image src={imgUrl}></Image>
          <View className='orderSettlement-header-imgMess__body marginLeft20'>
            <View className='bold fontSize36'>V02 (棋牌室 茶室)</View>
            <View className=''>
              <View className='app-gary app-text-hidden' style={{width: '11em'}}>
                广东省惠州市博罗县罗阳街道赛头创享大厦A栋502室
              </View>
              <View className='app-self-support marginLeft10'>自营</View>
            </View>
            <View className='app-red fontSize28'>会员
              <Text className='bold'>￥</Text>
              <View className='bold fontSize40'>16.8</View>
              <Text className='bold'>/</Text>每小时
            </View>
          </View>
        </View>

        <View className='app-separate marginTop30'></View>

        <View className='orderSettlement-header-record'>
          <View>
            <View className='app-gary2'>服务时间</View>
            <View>2023-12-16 00:30至2023-12-16 2:30</View>
          </View>
          <View>
            <View className='app-gary2'>时长费</View>
            <View>￥67.2</View>
          </View>
          <View>
            <View className='app-gary2'>服务时长</View>
            <View>共2小时</View>
          </View>
          <View>
            <View className='app-gary2'>优惠</View>
            <View>￥0.00</View>
          </View>
        </View>
      </View>

      <View className='orderSettlement-main'>
        <View className='orderSettlement-main-vip'>
          <View className='flex alignCenter'>
            <MIcon svg={vipSvg} width='2em' height='2em'></MIcon>
            <Text className='marginLeft20 fontSize36 bold'>开通会员享全场特惠</Text>
          </View>
          <View className='fontSize32'>
            去了解 <MIcon svg={rightGarySvg} width='1em' height='1em'></MIcon>
          </View>
        </View>

        <View className='orderSettlement-payment'>
          <View className='app-gary fontSize32'>支付方式</View>
          <View className='orderSettlement-payment-type border-bottom' onClick={handleCheckPayment.bind(null, 'cx')}>
            <View className='flex alignCenter'>
              <MIcon svg={paymentSvg} width='2em' height='2em'></MIcon>
              <Text className='marginLeft20 fontSize32'>店铺储值卡</Text>
            </View>
            <View>
              { paymentType === 'cx' ? <MIcon svg={checkSvg} width='1.5em' height='1.5em'></MIcon>: <View className='orderSettlement-payment-default'></View>}
            </View>
          </View>
          
          <View className='orderSettlement-payment-type' onClick={handleCheckPayment.bind(null, 'wx')}>
            <View className='flex alignCenter'>
              <MIcon svg={wxPaySvg} width='2em' height='2em'></MIcon>
              <Text className='marginLeft20 fontSize32'>微信支付</Text>
            </View>
            <View>
              { paymentType === 'wx' ? <MIcon svg={checkSvg} width='1.5em' height='1.5em'></MIcon>: <View className='orderSettlement-payment-default'></View>}
            </View>
          </View>
        </View>
        
        {
          paymentType === 'wx' && (
            <View className='orderSettlement-coupon'>
              <View>优惠券</View>
              <View>暂无可用优惠券 <MIcon svg={rightGarySvg} width='1em' height='1em'></MIcon></View>
            </View>
          )
        }

        <View className='orderSettlement-prompt'>
          <View className='orderSettlement-prompt-title fontSize32'>温馨提示:</View>
          {
            Array(10).fill(0).map((_, i) => (
              <View className='orderSettlement-prompt-item'>
                {`${i+1}.未成年禁止入位。`}
              </View>  
            ))
          }
        </View>
      </View>

      <View className='orderSettlement-footer'>
        <View className='fontSize32 app-gary2 flex alignCenter'>实付: <Text className='bold fontSize40 app-red'>￥67.80</Text></View>
        <View className='orderSettlement-footer-but'>
          去支付
        </View>
      </View>
    </View>
  )

  function handleCheckPayment (type) {
    updatePaymentType(type)
  }
}
