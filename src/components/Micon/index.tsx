import {Image} from "@tarojs/components";
import {Base64} from "js-base64";

type IconProps = {
  svg: string,
  width: string,
  height: string,
  style: any
}

function MIcon ({ svg, width, height, style }: Partial<IconProps> = { svg: '',  width: '40px', height: '40px', style: {} }) {
  const imgSrc = `data:image/svg+xml;base64,${Base64.encode(svg || '')}`;

  return (
    <Image src={imgSrc} style={{...style, width, height }} className="app-icon"></Image>
  );
}

export default MIcon;
