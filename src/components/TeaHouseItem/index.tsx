// 茶室列表子项
import { View, Image, Text, Picker } from '@tarojs/components'
import { memo, useEffect, useMemo, useState } from 'react'
import { AtFloatLayout, AtList, AtListItem } from "taro-ui"
import moment from 'moment';
import { dateSvg } from '../../assets/icons';
import Taro from '@tarojs/taro'
import MIcon from '../Micon';

const weekObj = {
  'Sunday': '星期天',
  'Monday': '星期一',
  'Tuesday': '星期二', 
  'Wednesday': '星期三', 
  'Thursday': '星期四', 
  'Friday': '星期五', 
  'Saturday': '星期六'
}


// const TimeModel = () => {

//   return (
// 123

//   )
// }



// 茶室列表子项
function TeaHouseItem () {

  const [orderTimeState, updateOrderTimeState] = useState<boolean>(false);
  const [pickerList, updatePickerList] = useState(moment());
  const [checkDate, updateCheckDate] = useState(pickerList.format('MM/DD'));
  const [checkTime, updateCheckTime] = useState([]);

  useEffect(() => {
    updateCheckTime([]);
  }, [checkDate])

    return (
      <View className='tea-house'>
        <View className='tea-house-left'>
          <Text className='tea-house_header__mark'>3.0小时起订</Text>
          <Image className='tea-house_header__brand' src='https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f'></Image>
        </View>
        <View className='tea-house-right'>
          <View  className='tea-house-title'>201（棋牌室）</View>
          <View className='tea-house-time'>
            <Text>会员￥<Text style={{fontWeight: 'bold'}}>12.8</Text>/小时</Text>
            <Text>非会员25.60/小时</Text>
          </View>
          <View className='tea-house-button'>
            <View>1-5人使用</View>
            <View>
              <View className='app-button app-button-default' onClick={handleOpenOrderTime}>提前预约</View>
              <View className='app-button app-button-confirm'>马上使用</View>
            </View>
          </View>
        </View>

        <AtFloatLayout
          isOpened={orderTimeState} 
          title='选择预约时间'
          onClose={() => updateOrderTimeState(!orderTimeState)}
          scrollWithAnimation={true}
          className="order-model-box"
        >
            <View className='order-model'>
              <View className='order-model-header'>
                {
                  useMemo(() => {

                    return (
                      Array(3).fill(0).map((_, i) => {
                        let week = pickerList.clone().add(i, 'days');
                        let weekStr = week.format('MM/DD');
                        return (
                          <View className={`order-model-header__item ${checkDate===weekStr?'order-model-check':''}`} onClick={handleCheckDate.bind(null, weekStr)}>
                            <View>{weekStr}</View>
                            <View>{weekObj[week.format('dddd')]}</View>
                          </View>
                        )
                      })
                    )
                  }, [pickerList, checkDate])
                }
                
                <View className={`order-model-header__item ${checkDate==='all'?'order-model-check':''}`} onClick={updateCheckDate.bind(null, 'all')}>
                  <Picker mode='date' start={moment().format('YYYY-MM-DD')} end={moment('2999-01-01').format('YYYY-MM-DD')} onChange={(v) => {
                    console.log(v.detail.value, moment(v.detail.value).format('MM/DD'), 'wwwwwwwwwwwwwwwwwww');
                    updatePickerList(moment(v.detail.value));
                    updateCheckDate(moment(v.detail.value).format('MM/DD'))
                  }}>
                    <MIcon svg={dateSvg} width='1.5em' height='1.5em'></MIcon>
                    <View>其他日期</View>
                  </Picker>
                </View>
              </View>
              <View className='order-model-main'>
                {Array(10).fill(0).map((_, i) => (
                  <View className={checkTime.includes(i)? 'order-model-main-time order-model-main-time__check': 'order-model-main-time'} onClick={handleCheckTime.bind(null, i)}>
                    <View>00:00 ~ 00:30</View>
                    <View>￥ 15.8</View>
                  </View>
                ))}
              </View>
              <View className='order-model-footer'>
                <View>取消</View>
                { checkTime.length? (
                  <View onClick={handleNavigatePay}>确定</View>  
                ): <View>选择预约时间</View> }
              </View>
            </View>
        </AtFloatLayout> 
      </View>
    )

    function handleOpenOrderTime () {
      updateOrderTimeState(true)
    }


    function handleCheckDate (week) {
      updateCheckDate(week)
    }

    // 选择预约时间
    function handleCheckTime (time) {
      if (checkTime.includes(time)) {
        updateCheckTime(arr => arr.filter(l => l != time))
      }
      else {
        updateCheckTime(arr => arr.concat(time))
      }
    }

    function handleNavigatePay () {
      Taro.navigateTo({
        url: `/pages/orderSettlement/index`
      })
    }
}


export default memo(TeaHouseItem);