import { View, Text, Image } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import MIcon from "../../components/Micon";
import './index.less'
import { obligationSvg, toBeUseSvg, inUseSvg, doneSvg, allSvg, editSvg } from '../../assets/icons'

const imgUrl = 'https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f';

const orderList = [
  { label: '待付款', icon: 'obligation', svg: obligationSvg },
  { label: '待使用', icon: 'toBeUse', svg: toBeUseSvg },
  { label: '使用中', icon: 'inUse', svg: inUseSvg },
  { label: '已完成', icon: 'done', svg: doneSvg },
  { label: '全部', icon: 'all', svg: allSvg }
]

const testIcon = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1682835743579" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="16627" width="16" height="16"><path d="M238.933333 643.657143l58.514286 53.638095 146.285714-146.285714-146.285714-146.285714-58.514286 53.638095 43.885715 43.885714H19.504762v97.52381h263.314286z" p-id="16628" fill="#B72D29"></path><path d="M794.819048 234.057143V92.647619H19.504762v316.952381h97.523809V331.580952h789.942858v131.657143h-243.809524v243.809524h243.809524v126.780952H117.028571v-136.533333h-97.523809v234.057143h984.990476V234.057143h-209.67619z m-677.790477 0v-43.885714h580.266667v43.885714H117.028571z m638.780953 375.466667v-48.761905h146.285714v48.761905h-146.285714z" p-id="16629" fill="#B72D29"></path></svg>'

export default function Index() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='my'>
      <View className='my-header'>
        <View>
          <Image src={imgUrl} className='my-header__img'></Image>
          <View>
            <Text className='marginRight10'>Hi, 神秘人</Text>
            <MIcon svg={editSvg} width='20px' height='20px'></MIcon>
          </View>
        </View>
      </View>

      <View className='my-main'>
        <View className='my-main__title'>我的钱包</View>
        <View className='my-main__body'>
          <View>
            <View>0</View>
            <View>余额</View>
          </View>
          <View>
            <View>0</View>
            <View>优惠券</View>
          </View>
          <View>
            <View>0</View>
            <View>茶元宝</View>
          </View>
          <View>
            <View>0</View>
            <View>押金</View>
          </View>
        </View>
      </View>

      <View className='my-footer'>
        <View className='my-footer-item'>
          <View className='my-footer-item__title app-title'>订单</View>
          <View className='my-footer-item__body'>
            { orderList.map(r => (
              <View className='my-footer-items_tag' onClick={handleNavigateOrder.bind(null, r)}>
                <MIcon svg={r.svg} width='25px' height='25px'></MIcon>
                <View>{ r.label }</View>
              </View>
            )) }
          </View>
        </View>
      </View>
    </View>
  )

  function handleNavigateOrder ({icon}) {
    console.log(icon)
    Taro.navigateTo({
      url: `/pages/orderForm/index?tab=${icon}`
    })
  }
}
