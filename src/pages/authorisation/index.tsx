import { View, Text } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import { AtMessage } from 'taro-ui'
import './index.less'


export default function Index() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='authorisation'>
      
      <View className='authorisation-title fontSize32'>登录后您可以进行以下操作:</View>

      <View className='app-gary2 authorisation-mess fontSize28'>
        <View className='flex alignCenter'>
          <View className='authorisation-mess-sign marginRight10'></View>
          <Text>在线预定</Text>
        </View>
        <View className='flex alignCenter marginRight10'>
          <View className='authorisation-mess-sign marginRight10'></View>
          <Text>预定信息查询</Text>
        </View>
      </View>

      <View className='authorisation-login' onClick={handleLogin}>微信用户一键登录</View>

      <View className='app-gary2 fontSize28'>暂不登录，我先逛逛</View>
      <AtMessage />
    </View>
  )

    function handleLogin () {

      Taro.login({
        success: function (res) {
          if (res.code) {
            Taro.atMessage({
              'message': '登录成功',
              'type': 'success',
            })
            setTimeout(() => {
              Taro.reLaunch({
                url: "/pages/index/index"
              })
            }, 800)
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      })

      
      
    }
}
