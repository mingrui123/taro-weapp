import { View, Text, Image, Input } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import { useState } from 'react'
import MIcon from '../../components/Micon'
import NullComponent from '../../components/NullComponent'


const rechargeList = [
  { amount: '59.00', give: '19.00' },
  { amount: '199.00', give: '59.00' },
  { amount: '399.00', give: '199.00' },
  { amount: '599.00', give: '399.00' },
]


export default function Index() {
  // 优惠列表
  const [checkRecharge, updateCheckRecharge] = useState();

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='recharge'>
      
      <View className='recharge-main'>
        <View className='recharge-main-title bold fontSize36'>充值</View>
        <View className='recharge-main-option'>
          {
            rechargeList.map(r => (
              <View 
                className={`${checkRecharge === r.amount? 'recharge-main-option__item-check': ''} recharge-main-option__item`}
                onClick={handleCheckRecharge.bind(null, r.amount)}
              >
                <View className='app-red'>
                  <Text className='bold fontSize40'>{r.amount}</Text>元
                </View>
                <View className='app-gary2 marginTop20'>送{r.give}元</View>
              </View>
            ))
          }
        </View>
        <View className='recharge-main-input'>
          <View>
            <Input placeholder='其他金额' value={checkRecharge} onInput={e => handleCheckRecharge(e.target.value)}></Input>
            <Text className='app-red marginLeft20'>元</Text>
          </View>
        </View>
      </View>

      <View className='recharge-footer'>
        <View>立即充值</View>
      </View>
    </View>
  )

  function handleCheckRecharge (amount) {
    updateCheckRecharge(amount)
  }
}
