import { View, Text } from '@tarojs/components'
import { useLoad } from '@tarojs/taro'
import './index.less'
import MIcon from '../..//components/Micon'
import { checkSvg, phone3Svg } from '../../assets/icons'

const labelList = ['沙发', '投影仪', 'WIFI', '麻将机', '智能售货机', '茶几', '纯净水', '小沙发', '空调暖气']

/**
 * 门店介绍
 * @returns 
 */
export default function Index() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='intro'>
      
      <View className='intro-header'>
        <View className='bold app-red fontSize32'>基本信息</View>
        <View>
          <Text className='app-gary2'>营业时间：</Text>
          <Text>00:00-24:00</Text>
        </View>
        <View>
          <Text className='app-gary2'>地址：</Text>
          <Text>广东省惠州市博罗县罗阳街道赛头创享大厦A栋502室</Text>
        </View>
        <View className='flex alignCenter'>
          <Text className='app-gary2'>联系电话：</Text>
          <Text className='marginRight20'>18823678967</Text> <MIcon svg={phone3Svg} width='2em' height='1.5em'></MIcon>
        </View>
      </View>

      <View className='intro-main'>
        <View className='bold app-red fontSize32'>标签</View>
        <View className='intro-main-label intro-main-body'>
          {labelList.map(res => (
            <View key={res} className='intro-main-label__item'>
              <View>
                <MIcon svg={checkSvg} width='15px' height='15px'></MIcon>
              </View>
              <Text>{res}</Text>
            </View>  
          ))}
        </View>

        <View className='bold app-red fontSize32'>简介</View>
        <View className='intro-main-body intro-main-label'>00</View>


        <View className='bold app-red fontSize32'>预约需知</View>
        <View className='intro-main-label intro-main-body'>
          <View className='bold app-red2 fontSize28'>1、订单生成后，若未使用或在使用时间内点击完成将视为以完成服务；</View>
        </View>
      </View>

    </View>
  )
}
