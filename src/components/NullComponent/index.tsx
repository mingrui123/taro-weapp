import { View } from '@tarojs/components';
import {memo} from 'react'
import MIcon from '../Micon';
import { nullSvg } from '../../assets/icons';

const NullComponent = ({text = '暂无数据'}: {text?: string}) => {

    return (
        <View className='null-view'>
            <MIcon svg={nullSvg} width='220rpx' height='220rpx'></MIcon>
            <View>{text}</View>
        </View>
    );
}


export default memo(NullComponent);