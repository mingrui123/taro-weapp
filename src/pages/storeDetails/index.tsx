import { View, Text, Image } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import { useState } from 'react'
import TeaHouseItem from '../../components/TeaHouseItem'
import { AtFloatLayout } from "taro-ui"
import MIcon from '../../components/Micon'
import { homeSvg, rightWhiteSvg, locationSvg, timeSvg, phone2Svg } from '../../assets/icons'

const imgUrl = 'https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f';


// const RoomComponent = () => {

//   return (
//     <View className='room'>
//       <View className='room-header'>
//         <Image src={imgUrl}></Image>

//         <View className='room-header-body'>
//           <View className='fontSize32 bold'>V01 (棋牌室 茶室)</View>
//           <View className=''>
//             <Text>会员<Text className='bold'>￥15.8/</Text>小时</Text>
//             <Text>非会员31.60/小时</Text>
//           </View>
//           <View>
//             <Text>1-5人使用</Text>

//           </View>
//         </View>
//       </View>

//       <View className='room-main'></View>

//       <View></View>
//     </View>
//   );
// }

const MoreStoreComponent = (props: {checkStore: () => void}) => {

  return (
    <View className='more-store' onClick={props.checkStore}>
      <View className='more-store__img'>
        <Image src={imgUrl}></Image>
      </View>
      <View className='more-store-mess'>
        <View className='bold fontSize32'>惠州博罗创想店</View>
        <View>
          <MIcon svg={locationSvg} width='20px' height='20px'></MIcon>
          <Text className='app-gary app-text-hidden more-store-mess_adress marginLeft10'>广东省惠州市博罗县罗阳街道赛头创享大厦A栋502室</Text>
        </View>
        <View>
          <MIcon svg={timeSvg} width='20px' height='17px'></MIcon>
          <Text className='app-gary marginLeft10'>营业时间: 00:00-24:00</Text>
        </View>
        <Text className='app-but_border' style={{width: 'auto'}}>距我626.1km</Text>
      </View>
    </View>  
  );
}


export default function Index() {
  // 门店列表状态
  const [storeListState, updateStoreListState] = useState<boolean>(true);

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='store-details'>
      
      <View className='store-details-tab'>
        <View className='marginRight20' onClick={handleNavigateHome}>
          <MIcon svg={homeSvg} width='23px' height='23px'></MIcon>
        </View>
        <View className='app-button app-button-confirm fontSize28 marginLeft20' style={{borderRadius: '20px', padding: '5px 10px'}} onClick={updateStoreListState.bind(null, true)}>
          切换门店 <MIcon svg={rightWhiteSvg} width='15px' height='15px'></MIcon>
        </View>
      </View>

      <View className='store-details-header' onClick={handleNavigateIntro}>
        <View className='store-details-header-img'>
          <Image src={imgUrl}></Image>
        </View>

        <View className='store-details-body'>
          <View>
            <View className='fontSize36 bold'>惠州博州罗创享店</View>
            <View className='fontSize28 app-gary'>已售298份</View>
          </View>
          <View>
            <View>
              <MIcon svg={locationSvg} width='20px' height='20px'></MIcon>
              <View className='fontSize28 app-text-hidden store-details-body-adress marginLeft20'>广东省惠州市博罗县罗阳街道赛头创享大厦A栋502室</View>
            </View>
            <View className='fontSize28 app-gary'>距您563.2km</View>
          </View>
          <View>
            <View>
              <MIcon svg={timeSvg} width='20px' height='17px'></MIcon>
              <Text className='fontSize32 marginLeft20'>营业时间00：00 ~ 24：00</Text>
            </View>
            <View className='fontSize28 app-gary app-button app-button-default'>
              <MIcon svg={phone2Svg} width='1em' height='1em'></MIcon> 联系商家
            </View>
          </View>
        </View>
      </View>

      <View className='store-details-main'>
        <View className='store-details-main-title app-red bold fontSize36'>茶室列表</View>
        <View>
          { Array(5).fill(0).map(() => (
            <TeaHouseItem></TeaHouseItem> 
          )) }
        </View>
      </View>

      <AtFloatLayout
        className='more-store-list'
        isOpened={storeListState} 
        title="切换商户" 
        onClose={() => updateStoreListState(!storeListState)}
        scrollTop={20}
        scrollWithAnimation={true}
        // lowerThreshold={20}
        onScrollToLower={() => {
          console.log('????')
        }}
      > 
        {/* <View className='more-store-list'> */}
          {Array(10).fill(0).map(() => (
            <MoreStoreComponent checkStore={checkStore}></MoreStoreComponent>
          ))}
        {/* </View> */}
      </AtFloatLayout>
    </View>
  )

  function handleNavigateIntro () {
    Taro.navigateTo({
      url: "/pages/intro/index"
    })
  }

  function checkStore () {
    updateStoreListState(false);
  }

  function handleNavigateHome () {
    Taro.reLaunch({
      url: "/pages/index/index"
    })
  }
}
