import { View, Input, Image, Text, Swiper, SwiperItem, ScrollView } from '@tarojs/components'
import { AtActionSheet, AtActionSheetItem, AtSearchBar  } from "taro-ui"
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import {useState} from "react";
import MIcon from "../../components/Micon";
import TeaHouseItem from '../../components/TeaHouseItem';
import { xiaSvg, shangSvg } from '../../assets/icons'

const main_list_mess = [
  {
    label: '开门码',
    key: 'openCode',
    icon: '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1682835424543" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="13341" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16"><path d="M51.5041 109.2157l367.120384 0 0 73.535488L271.792128 182.751188c-19.0403 84.352-44.8635 161.9036-77.4717 232.6405l191.632384 0 0 526.992384-69.439488 0 0-65.343488L190.367744 877.040584l0 77.599744-69.439488 0L120.928256 550.320128c-16.3523 29.952-34.048 57.1843-53.0883 81.6804-10.8964-19.0556-25.8724-40.832-44.928-65.3435 81.6476-114.3521 140.1436-242.3204 175.488-383.9037L51.504128 182.753328 51.504128 109.215744zM190.3677 480.7363l0 335.024128 126.144512 0L316.512212 480.736256 190.367744 480.736256zM422.6724 676.5117l420.656128 0 0 69.439488L422.672384 745.951188 422.672384 676.511744zM581.9515 182.7676l-24.4961 289.9681 232.783872 0 32.6717-326.8485L459.440128 145.8872 459.440128 76.447744l449.231872 0-44.928 396.288 106.191872 0c-5.4559 125.2475-10.8964 239.616-16.3359 343.0717 0 95.2955-51.7437 142.9443-155.1995 142.9443-35.3915 2.7197-80.3205 4.0796-134.7676 4.0796-2.7197-40.8484-6.8157-68.0796-12.2563-81.6804 8.1603 0 25.8724 0 53.1036 0 38.1123 2.7197 68.0479 4.0796 89.8396 4.0796 51.7284 2.7197 80.3205-23.1363 85.76-77.5997 5.4395-84.3837 8.1756-172.8799 8.1756-265.4556L479.855616 542.175244l28.5921-359.4076L581.951488 182.767644z" p-id="13342" fill="#B72D29"></path></svg>'
  },
  {
    label: '再续单',
    key: 'continuationOfOrders',
    icon: '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1682835508278" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="15482" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16"><path d="M828.2 131.4H702c-12.1-39.1-48.6-67.5-91.7-67.5H415.7c-43.1 0-79.6 28.4-91.7 67.5H196c-37.6 0-68 30.4-68 68v690.9c0 37.6 30.4 68 68 68h632.1c37.6 0 68-30.4 68-68V199.4c0.1-37.5-30.4-68-67.9-68zM513 724.3c0 17.6-14.3 31.9-31.9 31.9h-159c-17.6 0-31.9-14.3-31.9-31.9 0-17.6 14.3-31.9 31.9-31.9h159c17.6 0 31.9 14.3 31.9 31.9z m224.4-177.9c0 17.6-14.3 31.9-31.9 31.9h-385c-17.6 0-31.9-14.3-31.9-31.9 0-17.6 14.3-31.9 31.9-31.9h385.1c17.6 0 31.8 14.3 31.8 31.9z m0-177.9c0 17.6-14.3 31.9-31.9 31.9h-385c-17.6 0-31.9-14.3-31.9-31.9 0-17.6 14.3-31.9 31.9-31.9h385.1c17.6 0 31.8 14.3 31.8 31.9z" p-id="15483" fill="#B72D29"></path></svg>'
  },
  {
    label: '来充值',
    key: 'recharge',
    icon: '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1682835743579" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="16627" width="16" height="16"><path d="M238.933333 643.657143l58.514286 53.638095 146.285714-146.285714-146.285714-146.285714-58.514286 53.638095 43.885715 43.885714H19.504762v97.52381h263.314286z" p-id="16628" fill="#B72D29"></path><path d="M794.819048 234.057143V92.647619H19.504762v316.952381h97.523809V331.580952h789.942858v131.657143h-243.809524v243.809524h243.809524v126.780952H117.028571v-136.533333h-97.523809v234.057143h984.990476V234.057143h-209.67619z m-677.790477 0v-43.885714h580.266667v43.885714H117.028571z m638.780953 375.466667v-48.761905h146.285714v48.761905h-146.285714z" p-id="16629" fill="#B72D29"></path></svg>'
  },
]

function Item (props) {
  // 茶室列表状态
  const [listState, updateListState] = useState<boolean>(false)

  return (
    <View className='home-list__item'>
      <View className='home-item_header' onClick={handleNavigateDetails}>
        <View className='home-item_header__left'>
            <Text className='home-item_header__mark'>预定</Text>
            <Image className='home-item_header__brand' src='https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f'></Image>
        </View>
        <View className='home-item_header__right'>
          <View className='home-item_header__title'>【闲来】泉港界山惠华店</View>
          <View className='home-item_header__address'>
            <View className='home-item_header__address-left'>
              <View>
                <MIcon width='15px' height='15px' svg='<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg class="icon" width="16px" height="16.00px" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path fill="#2c2c2c" d="M511.425901 955.659138C481.601958 955.659138 168.221882 603.156422 168.221882 413.616259 168.221882 224.080961 321.866277 70.412239 511.425901 70.412239s343.20402 153.645612 343.20402 343.20402C854.629921 603.175883 539.724591 955.659138 511.425901 955.659138L511.425901 955.659138zM511.425901 114.69812C346.33602 114.69812 212.507762 248.544622 212.507762 413.616259c0 165.114207 290.551136 497.761864 298.918139 497.761864 3.853273 0 298.923004-332.647657 298.923004-497.761864C810.348905 248.544622 676.520648 114.69812 511.425901 114.69812L511.425901 114.69812zM511.425901 535.399389c-67.243749 0-121.783131-54.534516-121.783131-121.783131 0-67.243749 54.539382-121.783131 121.783131-121.783131 67.248614 0 121.783131 54.539382 121.783131 121.783131C633.209032 480.864873 578.674516 535.399389 511.425901 535.399389L511.425901 535.399389zM511.425901 336.119008c-42.799549 0-77.49725 34.717163-77.49725 77.49725 0 42.822659 34.697702 77.49725 77.49725 77.49725 42.803198 0 77.49725-34.674592 77.49725-77.49725C588.923151 370.836171 554.229099 336.119008 511.425901 336.119008L511.425901 336.119008zM511.425901 336.119008"  /></svg>'></MIcon>
              </View>
              <View className='marginLeft10'>福建省泉周市泉港区惠aa</View>
            </View>
            <View>
              <MIcon width='20px' height='20px' svg='<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg class="icon" width="16px" height="16.00px" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path fill="#2c2c2c" d="M685.92723544 633.48585572l-106.89842736 73.46216129c-112.02369443-55.40169638-203.79038094-145.94808119-260.90049967-256.9955343l80.78397136-109.33903076-53.93733435-156.19861533H179.99015797c0 175.47938191 68.3368942 340.22010903 192.31954514 464.20275996C496.29235402 772.6002475 661.03308115 840.93714173 836.51246305 840.93714173V679.12513863L685.92723544 633.48585572z"  /></svg>'></MIcon>
            </View>
          </View>
          <View className='home-item_header__time'>
            <Text>营业时间: 00:00-24:00</Text>
            <View>预订</View>
          </View>
        </View>
      </View>
      <View className='home-item_main'>
        <View>
          距我: 569.3km
        </View>
        <View>
          <View style={{display: 'flex', alignItems: "center"}} onClick={handleOpenMore}>查看茶室 <MIcon style={{marginLeft: '10px'}} width='20px' height='20px' svg={!listState? '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' +
            '<svg t="1682847342675" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="19385" width="16" height="16"><path d="M185.884 327.55 146.3 367.133 512.021 732.779 877.7 367.133 838.117 327.55 511.997 653.676Z" p-id="19386" fill="#B72D29"></path></svg>':
            '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' +
            '<svg t="1682847386224" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="20470" width="16" height="16"><path d="M838.116 732.779 877.7 693.195 511.979 327.549 146.3 693.195 185.883 732.779 512.003 406.652Z" p-id="20471" fill="#B72D29"></path></svg>'}/></View>
        </View>
      </View>
      {
        listState && (
          <View className='home-item_footer'>
            { Array(4).fill(1).map(() => <TeaHouseItem></TeaHouseItem>) }
          </View>
        )
      }
    </View>
  );

  function handleOpenMore (event) {
    event.stopPropagation()
    updateListState((boo) => !boo);
  }

  function handleNavigateDetails () {
    Taro.navigateTo({
      url: "/pages/storeDetails/index"
    })
  }
}


export default function Index() {

  const [actionTypeState, updateActionTypeState] = useState<boolean>(false);

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='home'>
      
      <View className='home_background__box'>
        <View className='home_header'>
          {/* <Input type='text' placeholder='查找附近'></Input> */}
          <AtSearchBar className='home_header-input'></AtSearchBar>   
        </View>

        <View className='home_main'>
          {
            main_list_mess.map(res => (
              <View key={res.key} className='home_main_child' onClick={handleNavigateTabType.bind(null, res)}>
                <View className='home_main_child__icon'>
                  <MIcon svg={res.icon} width='35px' height='35px' style={{color: 'red'}}/>
                </View>
                <Text>{ res.label }</Text>
              </View>
            ))
          }
        </View>

        <View className='home_swiper'>
          <Swiper
            indicatorColor='#999'
            indicatorActiveColor='#333'
            circular
            indicatorDots
          >
            <SwiperItem>
              <View className='home_swiper_item1 home_swiper_item'>1</View>
            </SwiperItem>
            <SwiperItem>
              <View className='home_swiper_item2 home_swiper_item'>demo2</View>
            </SwiperItem>
          </Swiper>
        </View>
      </View>


      <View className='home_footer'>
        <View className='home_footer_tab'>
          <View>全部</View>
          <View>共享茶室</View>
          <View className='flex alignCenter'>
            <Text onClick={handleOpenTypeFilter} className='marginRight10'>demo</Text>
            {
              !actionTypeState?<MIcon svg={xiaSvg} width='1em' height='1em'></MIcon>
              :<MIcon svg={shangSvg} width='0.9em' height='1em'></MIcon>
            }
          </View>
        </View>

        <ScrollView
          className='scrollview'
          scrollY
          scrollWithAnimation
        >
          { Array(8).fill(1).map(() => (<Item/>)) }
        </ScrollView>
      </View>


      <AtActionSheet
        isOpened={actionTypeState}
        onCancel={handleCancelTypeFilter}
        onClose={handleCancelTypeFilter}
      >
        <AtActionSheetItem>
          item1
        </AtActionSheetItem>
        <AtActionSheetItem>
          item2
        </AtActionSheetItem>
      </AtActionSheet>
    </View>
  )

  function handleOpenTypeFilter () {
    updateActionTypeState(true);
  }

  function handleCancelTypeFilter () {
    updateActionTypeState(false);
  }

  // tab 不同类型跳转
  function handleNavigateTabType (record) {
    let url = ''
    switch (record.key) {
      // 充值
      case "recharge": {
        url = '/pages/recharge/index'
        break
      }
    }

    Taro.navigateTo({
      url
    })
  }
}
