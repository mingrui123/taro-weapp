import { PropsWithChildren } from 'react'
import { Provider } from "react-redux"
import { useLaunch } from '@tarojs/taro'
import configStore from "./store"
// eslint-disable-next-line import/first
import "taro-ui/dist/style/index.scss"
import './app.less'

const store = configStore();

function App({ children }: PropsWithChildren) {

  useLaunch(() => {
    console.log('App launched.')
  })

  return (
    <Provider store={store}>
      { children }
    </Provider>
  )
}

export default App
