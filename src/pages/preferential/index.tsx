import { View, Text, Image } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './index.less'
import { useState } from 'react'
import MIcon from '../../components/Micon'
import NullComponent from '../../components/NullComponent'

const imgUrl = 'https://bkimg.cdn.bcebos.com/pic/4bed2e738bd4b31c2204c5ce8ed6277f9e2ff88f';


const PreferentialItem = () => {

  return (
    <View className='preferentialItem'>
      <View className='preferentialItem-label'>茶艺师优惠券</View>
      <View className='preferentialItem-body'>
        <View>
          <View className='fontSize32'>茶艺师20元体验卷</View>
          <View className='app-red fontSize32'>￥<Text className='bold fontSize40'>20.00</Text></View>
        </View>
        <View>
          <View>2023-05-11到期</View>
          <View className='fontSize28' style={{color: '#C68368'}}>满60.00使用</View>
        </View>
      </View>
      <View className='app-separate'></View>
      <View className='preferentialItem-footer'>
        <View className='app-gary2'>使用范围:茶艺师</View>
        <View className='preferentialItem-footer__but'>去使用</View>
      </View>
    </View>
  );
}


export default function Index() {
  // 优惠列表
  const [list, updateList] = useState([1]);

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className='preferential'>
     
     <View className='preferential-header'></View>

     <View className='preferential-main'>
        {
          list.length? <PreferentialItem></PreferentialItem>: <NullComponent></NullComponent>
        }
     </View>
    </View>
  )
}
