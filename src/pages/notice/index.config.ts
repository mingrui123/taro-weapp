export default definePageConfig({
  navigationBarTitleText: '共享茶室',
  navigationBarBackgroundColor: '#B72D29',
  navigationBarTextStyle: 'white'
})
